class Pay < ActiveRecord::Base
  attr_accessible :client_id, :month, :pay_type
  
 # scope :client_pays, lambda { |id| where("client_id = ?", id).order('month') }
  
  def self.get_margin
    @month_proc = 0.3
    @month_proc_exp = 0.5
    @summ_proc = 0 

    @data = self.where('pay_type <> 0')
    @data.each do |p|
        case p.pay_type # 0 - не платил, 1 - платил вовремя, 2 - платил с просрочкой, 3-досрочное погашение
          when 1 then
            @summ_proc += @month_proc
          when 2 then
            @summ_proc += @month_proc_exp
          when 3 then
            @summ_proc += @month_proc
        end
    end
    @cnt = @data.uniq.pluck(:client_id).count
    
    @margin = @summ_proc/(6*@cnt)
    
  end
  
end

class CalculatorController < ApplicationController
  def index
  end
  
  def calculate
    @summ = params[:n].to_i
    @profit1 = @summ * 0.3
    @margin = Pay.get_margin
    @profit2 = @summ * @margin
    respond_to do |format|
        format.js {}
    end

  end
end
